import Vue from 'vue'
import App from './App.vue'
// import Server from './Server.vue'  

new Vue({
  el: '#app',
  render: h => h(App)
})
